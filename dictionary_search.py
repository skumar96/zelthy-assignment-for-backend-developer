import requests
import json
class SearchDictionary:
    def _init_(self):
        pass

    def hit_api(self,word):

        url = f"https://api.dictionaryapi.dev/api/v2/entries/en_US/{word}"
        response = requests.request("GET", url)
        data = json.loads(response.text)
        result = list(filter(lambda x:x['partOfSpeech'] == 'noun' , data[0]['meanings']))[0]['definitions'][0]['definition']       
        print(f'{word}. Noun. {result}')

    def main(self):
        word = input('word? ')
        self.hit_api(word)

obj = SearchDictionary()
obj.main()
