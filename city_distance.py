from math import radians, sin, cos, acos
class Flight:
    def measureDistance(city1lt,city1lng,city2lt,city2lng):
    
        dist = 6371.01 * acos(sin(city1lt)*sin(city2lt) + cos(city1lt)*cos(city2lt)*cos(city1lng - city2lng))
        return dist

city_one_latitude=radians(float(input("city 1 latitude:")))
city_one_longitude=radians(float(input("City1 Longitude:")))
city_two_latitude=radians(float(input("city2 latitude:")))
city_two_longitude=radians(float(input("city 2 longitude:")))



d=Flight
result = d.measureDistance(city_one_latitude,city_one_longitude,city_two_latitude,city_two_longitude)
print("Distance of city1 of city2 is :",result)
